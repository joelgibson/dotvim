dotvim
======

My .vim configuration. It's pretty basic.

Installation
------------

Checkout the repository wherever you like. (I recommend `~/.vim`).

    git clone https://joelgibson@bitbucket.org/joelgibson/dotvim.git ~/.vim

Create a symlink to the `.vimrc` file:

    ln -s ~/.vim/vimrc ~/.vimrc

Backup files are saved in `~/tmp` to remove clutter. Make sure that directory exists.
Finally, install the Syntastic plugin:

    cd ~/.vim/bundle && git clone https://github.com/scrooloose/syntastic.git


Plugins
-------

[Syntastic](https://github.com/scrooloose/syntastic)
is a syntax checker. The following need to also be installed (if working in those
languages):

* Python: `pyflakes` (`pip install pyflakes`). Used to be using `flake8` but got
  too annoyed at it telling me how to do absolutely all formatting. No artistic license
* HTML: `tidy` (`npm install tidy`)
* Javascript: `JSHint` for Javascript (`npm install -g jshint`)
