" Load plugins
execute pathogen#infect()

" Yay UTF-8
set encoding=utf-8

" What does this even do
filetype plugin indent on

" Set up basic nice code stuff
syntax on

" Line numbers on side
set number

" Enable awesome
let mapleader=","

" Behave yourself, backspace!
set backspace=indent,eol,start

" Indentation etc
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set expandtab

" Always show statusline
set laststatus=2
set statusline=             " empty the status line
set statusline+=\ %n\ %*    " buffer number
set statusline+=%{&ff}%*    " file format
set statusline+=%y%*        " file type
set statusline+=\ %<%F%*    " full path
set statusline+=%m%*        " modified flag
set statusline+=%=          " hfill
set statusline+=%#warningmsg#                   " Syntastic stuff
set statusline+=%{SyntasticStatuslineFlag()}    " 
set statusline+=%*                              "
set statusline+=%5l%*       " current line
set statusline+=/%L%*       " total lines
set statusline+=%4c\ %*     " column number
set statusline+=0x%04B\ %*  " character under cursor

" Put swapfiles, backups elsewhere
set backupdir=~/tmp
set directory=~/tmp
set viminfo+=n~/tmp/.viminfo

" Behaviour
set splitright
set splitbelow

" How listchars are displayed
set listchars=tab:→\ ,trail:·

" Spelling
"
set spelllang=en

" ----- Pugins -----
"

"" Syntastic
let g:syntastic_check_on_open=1

" Use whatever cc and c++ are
let g:syntastic_c_compiler = 'cc'
let g:syntastic_cpp_compiler = 'c++'

" We always want up-to-date C, C++ options.
" let g:syntastic_c_compiler_options = '-std=gnu99'
let g:syntastic_cpp_compiler_options = '-std=c++0x'

" Python style checking is so tedious. I can run pylint manually
" if I want to be told off for writing one character variables.
" Pyflakes picks up on name errors (assigning but never using a var
" etc). Any syntax errors will also be picked up.
let g:syntastic_python_checkers=['pyflakes']

" Go - Does not work properly if error
"autocmd BufWritePre *.go :%!goimports <afile>

" ----- Filetypes -----"

"" Use tabs for Go, C, C++
autocmd BufRead,BufNewFile   *.go set noexpandtab
autocmd BufRead,BufNewFile   *.c set noexpandtab
autocmd BufRead,BufNewFile   *.h set noexpandtab
autocmd BufRead,BufNewFile   *.cpp set noexpandtab

"" .md defaults to Modula2 or something...
autocmd BufRead,BufNewFile   *.md set filetype=markdown

" ----- Custom commands -----
" Toggle the background from light to dark.
map <leader>b :call ToggleBackground()<CR>
function! ToggleBackground()
  if &background ==? "light"
    set background=dark
  else
    set background=light
  endif
endfunction

" Show trailing whitspace etc.
map <leader>l :set list!<CR>

" Toggle spelling
map <leader>s :set spell!<CR>

" Toggle line numbering
map <leader>n :set number!<CR>

" Show fonts
map <leader>f :set gfn=*<CR>

" ----- GVim -----"
source $VIMRUNTIME/mswin.vim
behave mswin
